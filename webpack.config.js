const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: __dirname + '/dist'
  },
  devServer: {
    port: 3333,
    static: {
      directory: path.resolve(__dirname, './static'),
      publicPath: '/static',
      serveIndex: true
    },
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    fallback: { crypto: false, path: false, fs: false, util: false }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
};