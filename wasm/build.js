const {exec} = require('child_process');
const path = require('path');

// const buildCommandWasm = `docker run --rm -v ${path.dirname(__dirname)}:/src:z -w /src emscripten/emsdk:latest /bin/bash /src/build.sh`;
const buildCommandWasm = `docker run --rm -v ${path.dirname(__dirname)}:/src:z -w /src/wasm emscripten/emsdk:latest /bin/bash build.sh`;
console.log('\x1b[33;1mRUNNING:\x1b[0m', '\x1b[32;1m' + buildCommandWasm + '\x1b[0m\n');

const processWasm = exec(buildCommandWasm, (error, stdout, stderr) => {
    if (error) {
        console.log(error);
    }
    else {
    }
})

processWasm.stdout.on('data', (data) => {
    console.log('\x1b[34;1m' + data.trim() + '\x1b[0m');
})


