#include <emscripten/bind.h>
#include <emscripten/val.h>
#include <iostream>
#include <stdio.h>
// #include <vector>
// #include <map>
// #include "SignalAnalyser.h"

using namespace emscripten;

void kdbush() {
    std::cout << "hej" << std::endl;
}

EMSCRIPTEN_BINDINGS(MyLib) {
//        register_vector<double>("vector<double>");
//        register_map<std::string, std::vector<double>>("map<string, vector<double>>");

    function("kdbush", &kdbush);

    // class_<SignalAnalyser>("SignalAnalyser")
    //     .constructor<int>()
    //     .function("shiftFrequency", &SignalAnalyser::shiftFrequency)
    //     .function("demodulate", &SignalAnalyser::demodulate)
    //     .function("ifft", &SignalAnalyser::ifft)
    //     .function("fft", &SignalAnalyser::fft)
    //     .function("getCompilationTime", &SignalAnalyser::getCompilationTime)
    // ;

    // enum_<ModulationType>("ModulationType")
    // .value("AM", ModulationType::AM)
    // .value("LSB", ModulationType::LSB)
    // .value("USB", ModulationType::USB)
    // .value("FM", ModulationType::FM)
    // .value("CW", ModulationType::FM)
    // .value("NONE", ModulationType::NONE)
    // ;
}