#define _USE_MATH_DEFINES
#include <fftw3.h>
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <complex>
#include <vector>
#include <deque>

using namespace std::complex_literals;

#ifndef NSG_SIGNALANALYSER_H
#define NSG_SIGNALANALYSER_H

#define Nfft 1024

enum ModulationType{AM, LSB, USB, FM, CW, NONE};

class SignalAnalyser {

public:
    SignalAnalyser(int bufferDepth);
    ~SignalAnalyser();
    std::string getCompilationTime();
    void fft(int nSamples, uint32_t sampleInPtr, uint32_t sampleOutPtr);
    void ifft(int nSamples, uint32_t sampleInPtr, uint32_t sampleOutPtr);
    void shiftFrequency(int nSamples, double fqShift, uint32_t sampleInPtr, uint32_t sampleOutPtr);
    double demodulate(int chunkNo, int modulation, int nSamples, double alphaAGC, uint32_t sampleInPtr, uint32_t audioSampleOutPtr, uint32_t iqSampleOutPtr);
    void AGC(int N, double* samples, double fs, double T, double W, double R, double Ta, double Tr, double* retSamples);

private:
    int bufferDepth;
    int sampleCount = 0;
    std::vector<std::complex<double>>* dataBuffer;
    double dcOffset = 0;
    double maxAverage = 1;
    double alphaOffset = 0.5;
    double gain = 0;
    double gainSmooth = 0;
    double* gaincomputer(int N, double* xdb, double T, double W, double R);
    double gs0 = -30.0;
    double OUTPUT_GAIN = 0.1;
    fftw_plan fftwPlanForward, fftwPlanBackward;
    double fftSamplesIn[Nfft*2];
    double (*fftIqSamplesIn)[2];
    fftw_complex fftIqSamplesOut[Nfft];
};


#endif //NSG_SIGNALANALYSER_H
