import {Supercluster} from './supercluster';
import moduleGenerator from '../wasm/geodensity_wasm';

const getRandomNumber = function (min, ref) {
    return Math.random() * ref + min;
}

moduleGenerator().then(module => {
    console.log(module);
    module.kdbush();


    const coordinates = [];
    for (let i = 0; i < 100000; i++) {
        coordinates.push([getRandomNumber(10, 10), getRandomNumber(55, 10)])
      }
      

  const features = coordinates.map(c => {
        return {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': c
            }
            // properties: {
            //     clusterFacility: facility
            // }
        }
    });

    //generate the cluster index
    self.index = new Supercluster({
        log: true,
        radius: 40,
        extent: 512,
        maxZoom: 17
    }).load(features);



})
